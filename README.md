# Getting Started



 checkout
 
 git clone git@gitlab.com:opolat/s2i-maven-builder.git

 make builder image
 
 make

 create example app
 
 s2i build --force-pull=false --loglevel=1 git@gitlab.com:opolat/hello-world.git s2i-maven-builder my-hello-world-image
 
 docker run -it my-hello-world-image    

 > Hello World !
 
 
 
deploy builder to openshift

oc new-app git@gitlab.com:opolat/s2i-maven-builder.git -n examples 


deploy app to openshift

oc new-app s2i-maven-builder:latest~https://opolat@gitlab.com/opolat/hello-world.git -n examples
